//"use strict";
let System = require("systemjs");

// Services
import { TwitchService } from "./server/twitch-server.service";
let twitchServerService = new TwitchService();

let path = require('path');
var express = require("express");
var app = express();
var rootPath = path.normalize(__dirname);

app.set("port", (process.env.PORT || 8080));

// for static files
app.use('', express.static(rootPath + "/public"));

app.get("/streams/:term", function (req, res) {
    console.log("searching...");
    var searchTerm = req.params.term;
    twitchServerService.retrieveStreamData(searchTerm, function(body, err){
        console.log("done");
        if (err) throw err;
        res.send(body);
    })
});

app.get('/dashboard', function (req, res) {
    res.send("Server dashboard Response");
});

// anything else, send the NG2 app
app.get("*", function(req, res) {
  console.log("sending application");
  res.sendFile(rootPath + "/public/index.html");
});

app.listen(app.get("port"), function() {
  console.log("Node app is running on port", app.get("port"));
});
