export const htmlTemplate = `
<h2>
    Single User Stream Info
</h2>

<div class="search-bar">
    <span><input [(ngModel)]="nameInput" placeholder="UserName" /></span>
    <span><button (click)="addUser()">Search User</button></span>
</div>

<div *ngIf="usersArr">
    <ul *ngFor="let i of usersArr">
        <li>Name: {{i.display_name}}</li>
    </ul>
    {{testStr}}
</div>
<img src="img/gears144.gif" alt="gears" height="42" width="42">
`;