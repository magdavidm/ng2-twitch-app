// The usual bootstrapping imports
import { bootstrap }    from "@angular/platform-browser-dynamic";
import { HTTP_PROVIDERS } from "@angular/http";

import { AppComponent } from "./components/app.component";
import { APP_ROUTER_PROVIDERS } from "./routes/app.routes";

import { TwitchSharedService } from "./services/twitch-shared.service";

document.addEventListener("DOMContentLoaded", function(event) {
    // TODO: register HTTP_PROVIDERS in AppComponent providers
    bootstrap(AppComponent, [
        APP_ROUTER_PROVIDERS,
        HTTP_PROVIDERS,
        TwitchSharedService
    ])
      .catch(err => console.error(err));
});
