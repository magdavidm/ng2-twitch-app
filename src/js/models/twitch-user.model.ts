export class TwitchUser {
    display_name: string;
    broadcaster_language: string;
    current_game: string;
    views: number;
    followers: number;
    logo: string;
    channel_link: string;
    fresh_data: boolean; // to flag data in the View as fresh or not (use w/ refresh)
    last_updated: string;
}