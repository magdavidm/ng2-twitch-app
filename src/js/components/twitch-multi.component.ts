import { Component } from "@angular/core";
import { RouteParams } from "@angular/router-deprecated";

import { TwitchUser } from "../models/twitch-user.model";
import { TwitchService } from "../services/twitch.service";
import { TwitchSharedService } from "../services/twitch-shared.service";
import { htmlTemplate } from "../templates/twitch-multi.html";

@Component({
    selector: "twitch-stream-multi",
    template: htmlTemplate
})

export class TwitchMultiComponent {
    constructor(
        private twitchService: TwitchService,
        private twitchSharedService: TwitchSharedService
    ) { }
    usersArr = this.twitchSharedService.getNameList();
    testStr = this.twitchSharedService.testStr;
    nameInput: string;
    singleUsr: TwitchUser;

    addUser() {
        const app = this;
        const user = new TwitchUser();
        const testUsr = {
            display_name: app.nameInput,
            status: "Unknown"
        };
        user.display_name = app.nameInput;
        app.usersArr.push(testUsr);
        console.log(app.usersArr.length);
    }
    getStreamData() {
        let app = this;
        for (let usrNum in app.usersArr) {
            app.usersArr[usrNum].status = "Unknown";
        }
        for (let usrNum in app.usersArr) {
            let urlBase = "https://ng2proto.herokuapp.com";
            // let urlBase = "";
            let url = urlBase + "/streams/" + app.usersArr[usrNum].display_name;
            // console.log("requesting data: " + url);
            this.twitchService.callAjax(url, function (msg) {
                let msgAsJSON = JSON.parse(msg);
                // console.log(msg);
                // console.log(msgAsJSON.stream);
                if (typeof (msgAsJSON.error) !== "undefined") {
                    app.usersArr[usrNum].status = msgAsJSON.message;
                }
                else {
                    if (msgAsJSON.stream) {
                        app.usersArr[usrNum].status = msgAsJSON.stream.game;
                    }
                    else {
                        app.usersArr[usrNum].status = "Offline";
                    }
                }


            });
        }
    }
}