import { Component } from "@angular/core";
import { ROUTER_DIRECTIVES } from "@angular/router";

import { TwitchService } from "../services/twitch.service";
import { TwitchMultiComponent } from "../components/twitch-multi.component";
import { TwitchSingleComponent } from "../components/twitch-single.component";
import { LineChartComponent } from "../components/line-chart.component";

@Component({
    directives: [ROUTER_DIRECTIVES],
    providers: [
        TwitchService,
    ],
    selector: "my-app",
    styleUrls: ["dist/css/component/app.component.css"],
    template: `
    <h1>{{title}}</h1>
    <nav>
    <a [routerLink]="['/single']">Single</a>
    <a [routerLink]="['/multi']">MultiStream</a>
    <a [routerLink]="['/line-chart']">LineChart</a>
    </nav>
    <router-outlet></router-outlet>
    `,
})
export class AppComponent {
    title = "TwitchTV App";
}
