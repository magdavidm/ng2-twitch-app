import { Component } from "@angular/core";
import { RouteParams } from "@angular/router-deprecated";

import { TwitchUser } from "../models/twitch-user.model";
import { TwitchService } from "../services/twitch.service";
import { TwitchSharedService } from "../services/twitch-shared.service";
import { htmlTemplate } from "../templates/twitch-single.html";

@Component({
    selector: "twitch-single",
    template: htmlTemplate
})

export class TwitchSingleComponent {
    constructor(
        private twitchService: TwitchService,
        private twitchSharedService: TwitchSharedService) { }
    usersArr = this.twitchSharedService.getUserList();
    testStr = this.twitchSharedService.testStr;
    nameInput: string;
    singleUsr: TwitchUser;

    addUser() {
        let app = this;
        let user = new TwitchUser();
        let testUsr = {
            display_name: app.nameInput
        };
        user.display_name = app.nameInput;
        app.usersArr.push(testUsr);
        console.log(app.usersArr.length);
    }
}