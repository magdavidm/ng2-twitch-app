import { provideRouter, RouterConfig } from "@angular/router";

import { TwitchMultiComponent } from "../components/twitch-multi.component";
import { TwitchSingleComponent } from "../components/twitch-single.component";
import { LineChartComponent } from "../components/line-chart.component";


export const routes: RouterConfig = [
  {
    component: TwitchMultiComponent,
    path: "",
  },
  {
    component: TwitchMultiComponent,
    path: "multi",
  },
  {
    component: TwitchSingleComponent,
    path: "single",
  },
  {
    component: LineChartComponent,
    path: "line-chart",
  },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
